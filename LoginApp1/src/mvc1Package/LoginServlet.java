package mvc1Package;
import dto.User;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.LoginService;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doPost(HttpServletRequest request,
						  HttpServletResponse response)
						  throws ServletException, IOException {
		
		String userID = request.getParameter("userID");
		String userPassword = request.getParameter("password");
		
		LoginService login = new LoginService();
		boolean result = login.authenticate(userID, userPassword);
		
		//przedresowywanie z servletu do strony JSP
		//wysylamy instrukcje do obiektu odpowiedzi nakazujace mu
		//przeadresowanie do innej strony, ktora wyswietli zawartosc
		if(result == true)
		{
			User user = login.getUserDetails(userID);
			
		//////1 sposob na przeniesienie strumienia z servletu do innego servletu/JSP
			
			//dodajemy model(obiekt user) do obiektu session aby byl dostepny we wszystkich
			//plikach jednej sesji(success.jsp, login.jsp itd), nie mozemy dodac obiektu do obiektu
			//application,gdyz spowodowalo by to systuacje w ktorej jeden uzytkownik
			//moze wejsc w posiadanie czyjegos konta
			//przekazujemy wiec obiekt user do zasiegu sesji
			//gdyz przeadresowanie do strony success.jsp tworzy nowy obiekt request 
			
			//request.getSession().setAttribute("user", user);
			
			//musimy przejsc do success.jsp uzywajac obiektu response
			//i metody sendRedirect bioracej za parametr nazwe pliku w
			//przypadku JSP, a dla Servletow podajemy w 
			//parametrze wartosc url-pattern
			//nie mozna jednoczesnie prznosic na wyjscie(odpowiedz) 
			//danych(uzywajac PrintWriter-a) oraz przeadresowywac  odpowiedzi
			
			//response.sendRedirect("success.jsp");
			
			
			
		//////2 sposob na przeniesienie strumienia z servletu do innego servletu/JSP
			
			//Uzywamy obiektu requestDispacher, ktory jest dostepny z obiektu request
			//Sama nazwa wskazuje ze WYSYLAMY zadanie
			//paramametr jest zasobem do ktorego chcemy wys�ac kontrole
			//przesylamy do wskazanego zasobu aktualny obiekt request i response, gdyz chcemy
			//aby przegladarka nie wiedziala ze tworzymy nowe zadanie
			//success.jsp otrzyma TEN SAM obiekt request i response ,ktory zostal powolany 
			//do zycia przez servlet
			//success.jsp otrzyma wszystkie parametry znajdujace sie w obiektach request i response
			//nie potrzebujemy teraz dodawania obiektu user do obiektu session, natomiast
			//mozemy wykorzystac obiekt request, ktory zostanie przekazany w funkcji forward do 
			//wskazanego zasobu
			
			request.setAttribute("user", user);
			
			RequestDispatcher dispatcher = request.getRequestDispatcher("success.jsp");
			dispatcher.forward(request, response);
			
			return;
			
		}
		else
			response.sendRedirect("login.jsp");
	}

}
