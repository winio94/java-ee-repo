package service;
import dto.User;
import java.util.HashMap;


public class LoginService {
	
	HashMap<String, String> users = new HashMap<String, String>();
	
	//konstruktor
	public LoginService() {
		users.put("johndoe", "John Doe");
		users.put("janedoe", "Jane Doe");
		users.put("jguru", "Java Guru");
	}
	
	public boolean authenticate(String ID, String passwd) {
		
		if(passwd == null || passwd == "")
			return false;
		else 
			return true;
	}
	
	public User getUserDetails(String ID) {
		User user = new User();
		user.setUserName(users.get(ID));
		user.setUserID(ID);
		return user;
	}
}
