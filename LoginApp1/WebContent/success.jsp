<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="dto.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Success</title>
</head>
<body>

<%
	//User user = (User)session.getAttribute("user");
	
	//uzywajac obiektu requestDispatcher nie potrzebujemy obiektu session
	
	//User user =  (User)request.getAttribute("user");
%>
						
						<!-- uzycie JSTL zamiast script tagow-->
<!-- ponizsza akcja jest rowna do tej zamieszczonej w powyzszym script tagu -->
<!-- JSTL automatycznie nadaje zmiennej nazwe atrybutu id, w tym przypadku user-->

<jsp:useBean id="user" class="dto.User" scope="request">
	<jsp:setProperty property="userName" name="user" value="Andrzej"/>
</jsp:useBean>


<h1>Login Successful!</h1>
<br>

<!-- dlatego ze JSTL automatycznie nadaje ziennej nazwe z atrybutu id, ponizsze polecenie dziala -->
<h3 align="center">Welcome 
	<%=user.getUserName() %>
</h3>
						
						<!-- kolejne uzycie JSTL zamiast powyzszego script tagu -->
<br>

<!-- getProperty uzywamy do pobierania wartosci ziarna definiowanego wczesniej -->
<!-- name mowi nam jaka jest nazwa ziarna -->
<!-- property to cecha ktora chcemy otrzymac(w tym przypadku pole klasy User(userName))  -->

<h3 align = "center">
	Welcome <jsp:getProperty property="userName" name="user"/>
</h3>


</body>
</html>