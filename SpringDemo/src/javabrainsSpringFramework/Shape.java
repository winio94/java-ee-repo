package javabrainsSpringFramework;

public interface Shape {
	public void draw();
}
