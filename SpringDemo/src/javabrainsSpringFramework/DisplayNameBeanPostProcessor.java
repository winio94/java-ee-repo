package javabrainsSpringFramework;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

//stworzylismy BeanPostProcessor, teraz musimy "poiwedziec" o tym Springowi
//ta czynnosc robimy w pliku xml
public class DisplayNameBeanPostProcessor implements BeanPostProcessor {

	@Override//mozemy zwroci dowolny obiekt
	public Object postProcessAfterInitialization(Object bean, String beanName)
			throws BeansException {
		
		System.out.println("In After initialization method. Bean name is " + beanName);
		return bean;
	}

	@Override										//obiekt, nazwa bean-a z pliku xml
	public Object postProcessBeforeInitialization(Object bean, String beanName)
			throws BeansException {
		
		System.out.println("In Before initialization method. Bean name is " + beanName);
		return bean;
	}
	
}
