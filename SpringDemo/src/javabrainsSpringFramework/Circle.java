package javabrainsSpringFramework;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

public class Circle implements Shape {
	
	private Point center;
	
	
	public Point getCenter() {
		return center;
	}

	//uzywamy adnotacji by "powiedziec" SPRING-owi ze dana zaleznosc (dependency) jest wymagana
	//Jesli ta metoda nie zostanie wywolana, SPRING nie dopusci do uruchomienia aplikacji
	//oraz zostanie wyloniony wyjatek
	//W pliku xml musimy zadeklarowac element beanPostProcessor, poniewaz to wlasnie ten element
	//okonuje walidacji oraz "zarzadza" adnotacjami, Wszystkie adnotacje sa sprawdzane przed 
	//inicjalizacja bean-ow, wiec jezeli ktoraz z adnotacji nie zostanie przetworzona pomyslnie
	//beanPostProcessor wyrzuci wyjatek
	
	//@Required
	
	//@Autowired
	//@Qualifier("cR")
	
	//JSR 250 annotation, dokonuje dependency injection przez nazwe
	@Resource(name="Point2")
	public void setCenter(Point center) {
		this.center = center;
	}


	public void draw() {
		System.out.println("Drawning a circle");
		System.out.println("Point  = (" + getCenter().getX() + ", " + getCenter().getY() + ")");
	}
	
	//wykonuje sie zaraz po utworzeniu bean-a
	@PostConstruct
	public void initCircle() {
		System.out.println("My init method called for Circle");
	}
	
	//wykonuje sie zaraz przed usunieciem bean-a
	@PreDestroy
	public void destroyCircle() {
		System.out.println("My destroy method called for Circle");
	}
	
}
