package javabrainsSpringFramework;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.SpringVersion;

public class DrawingApp {

	public static void main(String[] args) {
		
		//Triangle triangle = new Triangle();
		//triangle.draw();
		
		//uzywamy Springa zamiast polecenia new
		//wprowadzamy implementacje BeanFactory
		//BeanFacotry to interfejs i posiada wiele implementacji klas
		//do odczytywania pliku konfiguracji xml BeanFactiory musi byc rodzaju XMLBeanFactory
		//jako argument podajemy nazwe pliku xml
							//PRZESTARZALE
		//BeanFactory factory = new XmlBeanFactory(new FileSystemResource("spring.xml"));
		
		
		//klase wywyolujaca w ogole nie interesuje sposob w jaki Spring generuje obiekty
		//po prostu otrzymuje obiekt, od nas zalezy sposob generowania obiektu
		
		//AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		//context.registerShutdownHook();
		
		String version = SpringVersion.getVersion();
		System.out.println("Wersja Springa : " + version);
		
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
		context.registerShutdownHook();
		Shape shape = (Shape) context.getBean("circle");
		shape.draw();
		//Circle circle = (Circle) context.getBean("circle");
		//circle.draw();
		//triangle.draw();
		
		//((ClassPathXmlApplicationContext)context).close();
	}
}