package javabrainsSpringFramework;

import org.springframework.beans.factory.annotation.Required;

//import org.springframework.beans.factory.DisposableBean;
//import org.springframework.beans.factory.InitializingBean;

//import org.springframework.beans.BeansException;
//import org.springframework.beans.factory.BeanNameAware;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.ApplicationContextAware;

//import java.util.List;


//public class Triangle  implements InitializingBean, DisposableBean{

//interfejs InitializingBean:
//Zaznaczamy ze chcemy zaby metoda afterPropertiesSet()
//zostala wywolana podczas inicjalizacji bean-a 

//interfejs DisposableBean:
//zostanie wywolana metoda destroy() przed wlasciwym usuniecie bean-a

public class Triangle implements Shape {
	

	private Point PointA;
	private Point PointB;
	private Point PointC;
	
	
	public Point getPointA() {
		return PointA;
	}
	
	public void setPointA(Point pointA) {
		PointA = pointA;
	}

	public Point getPointB() {
		return PointB;
	}
	
	
	public void setPointB(Point pointB) {
		PointB = pointB;
	}

	public Point getPointC() {
		return PointC;
	}
	
	
	public void setPointC(Point pointC) {
		PointC = pointC;
	}

	public void draw() {
		System.out.println("Drawning Triangle");
		System.out.println("Point A = (" + getPointA().getX() + ", " + getPointA().getY() + ")");
		System.out.println("Point B = (" + getPointB().getX() + ", " + getPointB().getY() + ")");
		System.out.println("Point C = (" + getPointC().getX() + ", " + getPointC().getY() + ")");
		}

	/*	
	//konfiguracja w pliku spring.xml
	public void myInit () {
		System.out.println("My init method called for Triangle");
	}
	
	public void myDestroy() {
		System.out.println("My destroy method called for Triangle");
	}
	 */
	
	
	/*
	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("InitializingBeAN init method called for Triangle");
		
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("DisposableBean destroy method called for Triangle");
	}
*/

}


	//STARSZE

/*	@Override
public void setApplicationContext(ApplicationContext context)
		throws BeansException {
	this.context = context;
}

@Override
public void setBeanName(String beanName) {
	System.out.println("bean name is " + beanName);
}*/



//STARSZE
/*
private List<Point> Points;

	public List<Point> getPoints() {
		return Points;
	}

	public void setPoints(List<Point> points) {
		Points = points;
	}
	
	public void draw() {
		for(Point point : Points) {
			System.out.println("Point = (" + point.getX() + ", " + point.getY() + ")");
		}
		
	}
*/