package javaBrainsProjekt1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletyRecap extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) 
			throws ServletException, IOException {
			System.out.println("GET!");
			
			response.setContentType("text/html");
			
			String Name = request.getParameter("Name");
			String SurName = request.getParameter("SurName");
			String Email = request.getParameter("Email");
			String Sex = request.getParameter("Sex");
			String Message = request.getParameter("Message");
			String Age = request.getParameter("Age");
			
			HttpSession sesja = request.getSession();
			
			if(Name != "" & Name != null)
				sesja.setAttribute("savedName", Name);
			if(SurName != "" & SurName != null)
				sesja.setAttribute("savedSurName", SurName);
			if(Email != "" & Email != null)
				sesja.setAttribute("savedEmail", Email);
			if(Sex != "" & Sex != null)
				sesja.setAttribute("savedSex", Sex);
			if(Message != "" & Message != null)
				sesja.setAttribute("savedMessage", Message);
			if(Age != "" & Age != null)
				sesja.setAttribute("savedAge", Age);
			
			PrintWriter out = response.getWriter();
			out.println("<h2>HELLO " + (String) sesja.getAttribute("savedName") + " " + (String) sesja.getAttribute("savedSurName") + "</h2>");
			out.println("You have " + (String) sesja.getAttribute("savedAge") + ", your e-mail : " + (String) sesja.getAttribute("savedEmail"));
			out.println("Message, you wanted to send :  " + (String) sesja.getAttribute("savedMessage"));
			
	}
	
	protected void doPost(HttpServletRequest request, 
			HttpServletResponse response) 
			throws ServletException, IOException {
			System.out.println("POST!");
			
			response.setContentType("text/html");
			
			
	}
}
