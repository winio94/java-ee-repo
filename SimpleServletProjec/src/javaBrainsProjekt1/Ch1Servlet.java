package javaBrainsProjekt1;

import java.io.*;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(description = "HeadFirstServet1", urlPatterns = { "/Ch1ServletPath" })

//standardowe deklaracje servletu
public class Ch1Servlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, 
						HttpServletResponse response) 
						throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();//czesc strumienia odpowiedzi HTTP
		java.util.Date dzisiaj = new java.util.Date();
		//kod html osadzony w programie napisanym w j�zyku java
		out.println("<html> " + 
					"<body> " +
					"<h1 align=center>Nasz pierwszy servlet: Ch1Servlet</h1>"
					+ "<br>" + dzisiaj + "</body>" + "</html>");
		
	}

}
