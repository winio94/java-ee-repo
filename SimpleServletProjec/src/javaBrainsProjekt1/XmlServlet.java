package javaBrainsProjekt1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//ZAMIAST ANOTACJI przenosimy konfiguracje do deskryptora (web.xml)!!!!!!!!!!!!!!!

//w tym przypadku, jezeli chcemy uzyc deskryptora wdrozenia do odwzorowywania adresow url
//musimy stworzyc klase(new class zamiast servlet) oraz rozszerzyc ta klase
//do klasy HttpServlet
//nastepnie musimy odwzorowwac odpowienio nazwe wewnetrzna w publiczna
//w deskryptorze(web.xml)
public class XmlServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		//PRZEKAZYWANIE PARAMETRU
		//1 sposob, za pomoca adresu url i metoda GET
		
		PrintWriter writer = response.getWriter();
		String userName = request.getParameter("userName");
		writer.println("Hello from the method GET! " + userName);
		//writer.println("<h3 align = center>Hello, my name is " + userName + "my ID: " + userId + "</h3>");
	}
	
	
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//2 sposob
		response.setContentType("text/html");
		
		PrintWriter writer = response.getWriter();
		String userName = request.getParameter("userName");
		String fullName = request.getParameter("fullName");
		
		String proffession = request.getParameter("prof");
		//String location = request.getParameter("location");
		
		//Jesli chcemy uzyc jakiegos atrybutu ktory przesyla kilka wartosci jednoczesnie
		//jak np checkbox musimy uzyc metody getParameterValues
		String[] location = request.getParameterValues("location");
		
		writer.println("POST!");
		writer.println("Name : " + userName + ", full name : " + fullName);
		writer.println("Your proffesion is " + proffession);
		
		writer.println("You are at " + location.length + " places");
		
		for(String s:location)
			writer.println(s);
		
	}

}
