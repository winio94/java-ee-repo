package javaBrainsProjekt1;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.List;

import models.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UlubionePiwo extends HttpServlet {
	
	public void doPost(HttpServletRequest request, 
					   HttpServletResponse response) 
					   throws IOException, ServletException {
		
		PrintWriter writer  = response.getWriter();
		String wybranePiwo = request.getParameter("kolor");
		
		ModelPiwny model = new ModelPiwny();
		List wynik = model.getMarki(wybranePiwo);
		
		
		//dodajemy do obiektu zadania atrybut ktory bedzie wykorzystywany przez 
		//docelowa strone JSP
		request.setAttribute("styles", wynik);
		
		//Tworzymy obiekt przekazujacy zadanie do wlasciwej strony JSP
		RequestDispatcher view = request.getRequestDispatcher("piwo.jsp");
		
		//Wykorzystujemy obekt klasy RequestDispatcher do wymuszania na kontenerze
		//zaangazowania wskazanej strony JSP(w tym przekazania tej stronie
		//obiektow zadania i odpowiedzi)
		view.forward(request, response);
		
		
	}

}
