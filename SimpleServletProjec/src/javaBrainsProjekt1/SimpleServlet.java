package javaBrainsProjekt1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SimpleServlet
 */	
	//urlPatterns jest kluczowy!!
	//ponizsza anotacja(urlPatterns) umozliwia uruchomienie wlasciwego servletu
	//jesli ktokolwiek bedzie probowal otrzymac dostep do "/AdvancedServletPath", nastapi uruchomienie tej klasy
@WebServlet(description = "JavaBrainsOdcinek2", urlPatterns = { "/AdvancedServletPath" },
			initParams={@WebInitParam(name="defaultUser", value="John Doe")} )

//Jezeli chcemy zapamietac dane uzytkownika(aby nie byly tracone po kazdorazowym uruchomieniu strony=
//w pamieci servera musimy u�yc obiektu session, ktory jest udostepniany nam przez Tomcata
//mozemy zapisac pewne informacje w obiekcie session i uzyskac je w innej sesji
//aby uzyskac dostep do obiektu session musimy skozystac z metody obiektu request

//zeby napisac servlet musimy rozszerzyc klase o klase HttpServlet
public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		
		//parametr
		String userName = request.getParameter("userName");
		
		
		//uzyje obiektu session aby informacje o imieniu uzytkownika byly zapamietane  w serverze
		//po pierwszym wpisaniu ww informacji
		HttpSession session = request.getSession();//obiekt session udostepniany przez Tomcat-a
		
		//uzywam obiektu context, ktory w przeciwienstwie do obiektu session moze byc uzyty przez
		//roznych uzytkownikow/przegladarki(nie traci wartosci)
		//jest jeden obiekt context dla roznych aplikacji i wiele obiektow session
		ServletContext context = request.getServletContext();
		
		if(userName != "" && userName != null)
		{
			//mozemy rzechowywac dowolna wartosc w obiekcie session
			session.setAttribute("savedUserName", userName);
			
			context.setAttribute("savedUserName", userName);
		}
		
		
		
		PrintWriter writer = response.getWriter();
		writer.println("<h3>Request parameter has username as " + userName + "</h3>");
		
		//funkcja getAttribute zwraca obiekt Object, wiec nalezy go rzutowac na odpowiedni typ
		writer.println("<h3>Session(remembered) parameter has username as " + (String) session.getAttribute("savedUserName") + "</h3>");
		
		writer.println("<h3>Context(remembered) parameter has username as " + (String) context.getAttribute("savedUserName") + "</h3>");
		
		writer.println("<h3>Init parameter has default username as " + getServletConfig().getInitParameter("defaultUser") + "</h3>");
	}
}
