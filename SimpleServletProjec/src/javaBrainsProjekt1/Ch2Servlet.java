package javaBrainsProjekt1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(description = "HeadFirstServlet2", urlPatterns = { "/Ch2ServletPath" })

public class Ch2Servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, 
						HttpServletResponse response) 
						throws ServletException, IOException {
		java.util.Date data = new java.util.Date();
		
		response.setContentType("text/html");
		PrintWriter writer = response.getWriter();
		writer.println("<html>" +
					"<title>" +
					"Servlet numer 2" +
					"</title>" +
					"<body>" +
					"<h1 align = center>" + "Naglowek naszego drugiego servletu!" + "</h1>" +
					"<p align = left>" +
					"Jest godzina : " + data +"</p>" + "<br>" +
					"</body>" + "</html>"
					 );
	}
}
