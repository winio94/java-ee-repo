<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Konfiguracja parametru init</title>
</head>
<body>
<%!
	public void jspInit() {
	String defaultName = getServletConfig().getInitParameter("defaultName");
	
	ServletContext context = getServletContext();
	context.setAttribute("defaultName", defaultName);
	
	
}
%>


<!-- uzyskiwanie dostepu do parametrow domyslnych w script tagach -->
<br>
Default name : <%=getServletConfig().getInitParameter("defaultName") %>
<br>
The value in the Servlet context object : <%=getServletContext().getAttribute("defaultName") %>


</body>
</html>