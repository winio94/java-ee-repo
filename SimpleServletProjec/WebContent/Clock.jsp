<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.Date"%>
<%@ page import="java.awt.Checkbox" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Clock</title>
</head>
<body>

<!-- Tzw importow(klas/przesteni nazw) mozemy dokonywac w 
	 pierwszych linijkach pliku jsp -->
<!-- pierwsze dwie linie stanowia tzw dyrektywe strony, ktora posiada kazda
     strona JSP, mozemy wymieniac/precyzowac tam pewne wartosci/wlasnosci
     uzywane na stronie -->
<!-- Mozemy rowniez tworzyc wlasne dyrektywy pod dyrektywa stworzona automatycznie -->

<!-- Przydatna rzecza jest dyrektywa include, ktora umozliwia 
	 wlaczanie odpowiedzi(wyjscia) z innej strony JSP do strony w ktorej 
	 dyrektywa zostala uzyta -->
<%@ include file="/Hello.jsp" %>

<b>The time is: <%=new Date() %></b>

</body>
</html>