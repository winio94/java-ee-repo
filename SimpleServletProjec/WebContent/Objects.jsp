<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%
	//aby uzyskaz dostep do obiektu request
	String userName = request.getParameter("name");

	
	if(userName != null)
	{
		session.setAttribute("sessionUserName", userName);
		application.setAttribute("contextUserName", userName);
	}
	
	//dodatkowo strony JSP posiadaja obiekt PageContext Object niedostepny w servletach
	//Obiekt ten moze posluzyc nam do "ustawiania" obiektow request,
	//session lub application(context), wiec mozna go nazwac uniwersalnym
	pageContext.setAttribute("pageContextUserName", userName);
	//pageContext.setAttribute("contextUserName", userName, PageContext.APPLICATION_SCOPE);
	
	//funkcja findAttribute obiektu pageContext znajduje zasieg w ktorym 
	//podana wartosc jest ustawiona 
	//najpierw sprawdza czy jest atrybut w obiekcie pageContext o podanej nazwie
	//jesli nie ma to funkcja sprawdzi obiekt request, jesli nei znajdzie go tam 
	//to sprawdzi obiekt session, nastepnie obiekt appplication
	pageContext.findAttribute("name");
	
%>
<br>
The user name in request object is: <%=userName %>
<br>				<!--  aby uzyskac dostep do obiektu session -->
The user name in session object is: <%=session.getAttribute("sessionUserName") %>
<br>				<!--  aby uzyskac dostep do obiektu context -->
The user name in context object is: <%=application.getAttribute("contextUserName") %>
<br>				<!--  aby uzyskac dostep do obiektu pageContext -->
The user name in pageContext object is: <%=pageContext.getAttribute("pageContextUserName") %>


</body>
</html>