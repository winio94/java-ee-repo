<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Pierwsza strona JSP</title>
</head>
<body>
	<h3>Testing JSP</h3>
	
	                               <!--  -->
	<!-- Caly kod zawarty w script tagi jest kopiowany do tworzonej 
		 przez Tomcat-a klasy. konretnie do servletu a konretniej do metody
		 Service ktora decyduje czy wywolac metode doGet lub doPost
		 
		 Caly pozostaly kod(nie ujety miedzy script tagi) jest od razu 
		 przenoszony na wyjscie(przy uzyciu obiektu printWriter)
		 
		 Skoro caly kod miedzy script tagami jest tak na prawde 
		 w metodzie doGet, nie mozemy umieszczac miedzy standardowymi script tagami 
		 deklaracji METOD, gdyz w Javie nie mozna deklarowac/definiowac
		 metody w metodzie
		 Do tego zadania sa przeznaczone tagi deklaracyjne
		 --> 
		 
		 						   <!--  -->
		
	
	<!--  przydatnym tagiem jest tag deklaracyjny, mozemy
		  go wykorzystac np do zadeklarowania metody, co wiecej ta metoda bedzie
		  widoczna we wszystkich script tagach  -->
	
	<%! 
		public int add(int x, int y) {
			return x + y;
		}
	%>
	
	
	
	<!-- kod Javy znajduje sie miedzy dwoma tagami -->
	<%
	
	int i = 1;
	int j = 2;
	int k;
	k = i + j;
	//obiekt out jest typu JSPWriter wiec mozna przy jego pomocy 
	//wypisywac dane
	out.println("value of k = " + k); 
	
	%>
	
	<br>
	
	<!--  drugi sposob do wypisywania danych -->
	<!-- dynamiczna wartosc(zmienna/wyrazenie) zawieramy miedzy script tagi oraz znak " = " --> 
	value of k = <%=5*11 %>!!
	
	<br>
	<%
		k = add(12312, 4232);
		out.println("k is equal to : " + k);
	%>
	
	
	
	<%
		for(i = 0; i < 5 ; i ++) 
		{
	%>
		<br> The new value of i = <%=i %>
	
	<% 
		}
	%>
	
	
	
</body>
</html>